2017.03.24 Stable Release Version------>reversion:353

2017.04.13 Stable Release Version------>reversion:412

2017.06.16 Stable Release Version------>reversion:504
      ----->a,without file split function; b,waveform data in xor mode; c,waveform data won't include cycle-info

2017.08.02 Stable Release Version------>reversion:621
      ----->a,add file split function; b,add probe breakpoint function  c,modify the function of decoding probe waveform data

2017.12.01 HyperBlue-Semu_V2.1.1,detail as follow:
	1,modify the problem of abnormal waveform data in case of probe-breakpoint
	2,optimize the performance of probe waveform
	3,waveform file split function redo

2017.12.01 HyperBlue-Semu_V2.1.2,detail as follow:
	1,modify the problem of abnormal waveform data in case of probe-breakpoint
	2,modify the problem of abnormal waveform data when switch the readback status.(start dump <-> stop dump)


2019.01.25 HyperBlue-Semu_V2.1.4,detail as follow:
    1,delete ISE tools check;
    2,modify the JTAG bit load from ISE impact to  vivado HardWare;
    3,modify the problem of stop at the wrong breakpoint while using the probe-breakpoint,the data from dut is registered by start_to_dump in module "split_top"
