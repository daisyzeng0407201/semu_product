`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/30/2015 04:39:32 AM
// Design Name: 
// Module Name: data_ds
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module data_ds(
    data_in_p,
    data_in_n,
    data_in_s,
    data_out_s,
    data_out_p,
    data_out_n
);
    parameter  IN_DATA_WIDTH  = 32;
    parameter  OUT_DATA_WIDTH = 32;

    input  [IN_DATA_WIDTH-1:0]  data_in_p;
    input  [IN_DATA_WIDTH-1:0]  data_in_n;
    output [IN_DATA_WIDTH-1:0]  data_in_s;
    input  [OUT_DATA_WIDTH-1:0] data_out_s;
    output [OUT_DATA_WIDTH-1:0] data_out_p;
    output [OUT_DATA_WIDTH-1:0] data_out_n;
    
    // ---------------------------------------------------------------------------
    // Signal Declaration
    // ---------------------------------------------------------------------------
    
    wire  [OUT_DATA_WIDTH-1:0]  data_out_d;

    // ---------------------------------------------------------------------------
    // Data In Process
    // ---------------------------------------------------------------------------
                       
    generate  
        genvar i;
	
	    for(i=0;i<=IN_DATA_WIDTH-1;i=i+1) begin
        
            IBUFDS
            #(
                .DQS_BIAS("FALSE") // (FALSE, TRUE)
            )
            data_in_buf
            (
                .I  ( data_in_p[i] ),
                .IB ( data_in_n[i] ),
                .O  ( data_in_s[i] )
            );
        end
    endgenerate

    // ---------------------------------------------------------------------------
    // Data Out Process
    // ---------------------------------------------------------------------------
            
    generate  
        genvar j;
	    
	    for(j=0;j<=OUT_DATA_WIDTH-1;j=j+1) begin
            // SDR Output Data
            ODDR
              #(
                .DDR_CLK_EDGE("SAME_EDGE")
                )
            data_out_a_ddio
              (
               .Q                       ( data_out_d[j] ),
               .C                       ( data_out_s[j] ),
               .CE                      ( 1'b1 ),
               .D1                      ( 1'b1 ),
               .D2                      ( 1'b0 ),
               .R                       ( 1'b0 ),
               .S                       ( 1'b0 )
               );
            
            OBUFDS
              #(
                .IOSTANDARD("DEFAULT")
                )
            data_out_a_obuf
              (
               .O                       ( data_out_p[j] ),
               .OB                      ( data_out_n[j] ),
               .I                       ( data_out_d[j] )
               );
        end
    endgenerate
    
endmodule
