// Copyright 2015--2016 HyperBlue Inc. All rights reserved.

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

// `define  BSV_TOP  mkBridge
// `define  SIMULATION

module fpga_a
  (
   // System Clock & Reset
   input                    SYS_CLK,
   input                    SYS_RST_N,

   // Reset From FPGA-Q
   input                    PCIE_IN_PERSTN,

   // Status
   output                   DDR_READY,
   output                   OUT_OF_RESET,

   // NOC-A clock outputs
   output                   NOC_CLK_AP,
   output                   NOC_CLK_AN,
   output                   NOC_CLK_A_LOCK,

   // NOC-Q clock inputs
   input                    NOC_CLK_QP,
   input                    NOC_CLK_QN,
   input                    NOC_CLK_Q_LOCK,

   input                    FPGA_CLK_P,
   input                    FPGA_CLK_N,
   
   // NOC training and data signaling
   input                    NOC_TRAIN_sequence_in_p,
   input                    NOC_TRAIN_sequence_in_n,
   output                   NOC_TRAIN_sequence_out_p,
   output                   NOC_TRAIN_sequence_out_n,

   input [1:0]              NOC_SOURCE_credit_return_count_p,
   input [1:0]              NOC_SOURCE_credit_return_count_n,
   output                   NOC_SOURCE_valid_p,
   output                   NOC_SOURCE_valid_n,
   output [31:0]            NOC_SOURCE_beat_p,
   output [31:0]            NOC_SOURCE_beat_n,

   output [1:0]             NOC_SINK_credit_count_p,
   output [1:0]             NOC_SINK_credit_count_n,
   input                    NOC_SINK_valid_p,
   input                    NOC_SINK_valid_n,
   input [31:0]             NOC_SINK_beat_p,
   input [31:0]             NOC_SINK_beat_n,

   input [ 3:0]             leds_in,

`ifdef DDR3_SODIMM_STYLE
   inout [63 : 0]           DDR3_DQ,
   inout [7 : 0]            DDR3_DQS_P,
   inout [7 : 0]            DDR3_DQS_N,
   output                   DDR3_CLK_P,
   output                   DDR3_CLK_N,
   output [14 : 0]          DDR3_A,
   output [2 : 0]           DDR3_BA,
   output                   DDR3_RAS_N,
   output                   DDR3_CAS_N,
   output                   DDR3_WE_N,
   output                   DDR3_RESET_N,
   output                   DDR3_CS_N,
   output                   DDR3_ODT,
   output                   DDR3_CKE,
   output [7 : 0]           DDR3_DM,
`endif
   
   output [3:0]             FPGA_LED
   );

   wire                     noc_clk_a, noc_clk_a_pre, noc_clk_q, noc_clk_a_n, noc_clk_q_pre, noc_clk_a_d, noc_clk_q_fb;
   wire                     core_reset, core_reset_n;
   wire                     sys_clk;
   reg                      sys_rstn, sys_rstn_pre;
   wire                     fpga_clk;
   
   wire                     noc_train_sequence_in_s;
   wire                     noc_train_sequence_out_s;
   wire [ 1:0]              noc_source_credit_return_count_s;
   wire                     noc_source_valid_s;
   wire [31:0]              noc_source_beat_s;
   wire [ 1:0]              noc_sink_credit_count_s;
   wire                     noc_sink_valid_s;
   wire [31:0]              noc_sink_beat_s;

   wire    clkgen_pll_CLKFBOUT;
   wire    clkgen_pll_CLKFBIN;
   wire    clkgen_pll_CLKOUT0B;
   wire    clkgen_pll_CLKOUT0;
   wire    clkgen_pll_locked;
   wire    debounced_rst_n;
   
   assign core_reset_n = debounced_rst_n & PCIE_IN_PERSTN & NOC_CLK_Q_LOCK & NOC_CLK_A_LOCK;
   assign core_reset   = ~core_reset_n;
   assign noc_clk_q    = noc_clk_q_pre;
   assign noc_clk_a    = clkgen_pll_CLKOUT0;
   assign NOC_CLK_A_LOCK = clkgen_pll_locked;

   // Source Synchronous Clock from FPGA-Q
   IBUFDS  noc_clk_a_ibuf
     (
      .O                    (noc_clk_q_pre),
      .I                    (NOC_CLK_QP),
      .IB                   (NOC_CLK_QN)
      );
/*
   PLL_ADV
     #(
       .CLKFBOUT_MULT       (8), // 1000 MHz VCO
       .CLKFBOUT_PHASE      (0),
       .CLKIN1_PERIOD       (8), // 125 MHz reference
       .CLKOUT0_DIVIDE      (8), // 125 MHz clkout0
       .CLKOUT0_PHASE       (0)
       )
   noc_clk_a_pll
     (
      .CLKIN1               (noc_clk_q_pre),
      .CLKINSEL             (1'b1),
      .CLKFBIN              (noc_clk_q_fb),
      .RST                  (~NOC_CLK_Q_LOCK),
      .CLKOUT0              (noc_clk_a_pre),
      .CLKFBOUT             (noc_clk_q_fb),
      .LOCKED               (NOC_CLK_A_LOCK)
      );

   BUFG noc_clk_a_buf
     (
      .O                    (noc_clk_a),
      .I                    (noc_clk_a_pre)
      );
*/   

`ifndef  SIMULATION  
   key_debouncing  U_key_debouncing
   (
       .clk(sys_clk),
       .rst_n(1'b1),
       .key_in(SYS_RST_N),
       .key_out(debounced_rst_n)
   );
`else   
   assign  debounced_rst_n = SYS_RST_N;
`endif
   
   BUFG  clkgen_pll_clkfbbuf
    (
        .I(clkgen_pll_CLKFBOUT),
        .O(clkgen_pll_CLKFBIN)
    );

    MMCME2_ADV #(.BANDWIDTH("OPTIMIZED"),
	       .CLKFBOUT_USE_FINE_PS("FALSE"),
	       .CLKOUT0_USE_FINE_PS("FALSE"),
	       // .CLKOUT1_USE_FINE_PS("FALSE"),
	       // .CLKOUT2_USE_FINE_PS("FALSE"),
	       // .CLKOUT3_USE_FINE_PS("FALSE"),
	       // .CLKOUT4_CASCADE("FALSE"),
	       // .CLKOUT4_USE_FINE_PS("FALSE"),
	       // .CLKOUT5_USE_FINE_PS("FALSE"),
	       // .CLKOUT6_USE_FINE_PS("FALSE"),
	       .COMPENSATION("ZHOLD"),
	       .STARTUP_WAIT("FALSE"),
	       .CLKFBOUT_MULT_F(50.0),
	       .CLKFBOUT_PHASE(0.0),
	       .CLKIN1_PERIOD(50.0),
	       .CLKIN2_PERIOD(0.0),
	       .DIVCLK_DIVIDE(32'd1),
	       .CLKOUT0_DIVIDE_F(20),  // 8->125MHz
	       .CLKOUT0_DUTY_CYCLE(0.5),
	       .CLKOUT0_PHASE(0.0),
	       .REF_JITTER1(1.0e-2),
	       .REF_JITTER2(1.0e-2)) U_CLKGEN_PLL(.CLKIN1(sys_clk),
						      .RST(~debounced_rst_n),
						      .CLKIN2(1'd0),
						      .CLKINSEL(1'd1),
						      .DADDR(7'd0),
						      .DCLK(1'd0),
						      .DEN(1'd0),
						      .DI(16'd0),
						      .DWE(1'd0),
						      .PSCLK(1'd0),
						      .PSEN(1'd0),
						      .PSINCDEC(1'd0),
						      .PWRDWN(1'd0),
						      .CLKFBIN(clkgen_pll_CLKFBIN),
						      .LOCKED(clkgen_pll_locked),
						      .CLKFBOUT(clkgen_pll_CLKFBOUT),
						      .CLKFBOUTB(),
						      .CLKOUT0(clkgen_pll_CLKOUT0),
						      .CLKOUT0B(clkgen_pll_CLKOUT0B)
						      );

   IBUFDS  fpga_clk_ibuf
     (
      .O                    (fpga_clk),
      .I                    (FPGA_CLK_P),
      .IB                   (FPGA_CLK_N)
      );
   
   
   // G0 clock
   IBUFG sys_ibuf
     (
      .O                    (sys_clk),
      .I                    (SYS_CLK)
      );
      
   always @(posedge sys_clk, negedge core_reset_n) begin
      if (!core_reset_n) begin
         sys_rstn_pre <= 0;
         sys_rstn <= 0;
      end
      else begin
         sys_rstn_pre <= 1;
         sys_rstn <= sys_rstn_pre;
      end
   end
         
   // BSV Generated Code
   `BSV_TOP top
     (
      .CLK_sys_clk                    (sys_clk),
      .RST_N_sys_rstn                 (sys_rstn),

      .CLK_noc_q_clk                  (noc_clk_q),
      .CLK_noc_a_clk                  (noc_clk_a),
      .RST_N_noc_reset_n              (core_reset_n),

      .CLK_fpga_clk                   (fpga_clk),

      .isDDRReady                     (DDR_READY),
      .isOutOfReset                   (OUT_OF_RESET),
      
      .TRAIN_sequence_in_b            (noc_train_sequence_in_s),
      .TRAIN_sequence_out             (noc_train_sequence_out_s),
      .NOC_SOURCE_credit_return_count (noc_source_credit_return_count_s),
      .NOC_SOURCE_beat                ({ noc_source_valid_s, noc_source_beat_s }),
      .NOC_SINK_credit_count          (noc_sink_credit_count_s),
      .NOC_SINK_beat_b                ({ noc_sink_valid_s, noc_sink_beat_s })

`ifdef DDR3_SODIMM_STYLE
      .DDR3_DQ                        (DDR3_DQ),
      .DDR3_DQS_P                     (DDR3_DQS_P),
      .DDR3_DQS_N                     (DDR3_DQS_N),
      .DDR3_CLK_P                     (DDR3_CLK_P),
      .DDR3_CLK_N                     (DDR3_CLK_N),
      .DDR3_A                         (DDR3_A),
      .DDR3_BA                        (DDR3_BA),
      .DDR3_RAS_N                     (DDR3_RAS_N),
      .DDR3_CAS_N                     (DDR3_CAS_N),
      .DDR3_WE_N                      (DDR3_WE_N),
      .DDR3_RESET_N                   (DDR3_RESET_N),
      .DDR3_CS_N                      (DDR3_CS_N),
      .DDR3_ODT                       (DDR3_ODT),
      .DDR3_CKE                       (DDR3_CKE),
      .DDR3_DM                        (DDR3_DM),
`endif

      // ,.leds                           (FPGA_LED)
      );

   // SDR Output Clock  
   ODDR
     #(
       .DDR_CLK_EDGE("SAME_EDGE")
       )
   noc_clk_a_ddio
     (
      .Q                       ( noc_clk_a_d ),
      .C                       ( clkgen_pll_CLKOUT0B ),
      .CE                      ( 1'b1 ),
      .D1                      ( 1'b1 ),
      .D2                      ( 1'b0 ),
      .R                       ( 1'b0 ),
      .S                       ( 1'b0 )
      );

   OBUFDS
     #(
       .IOSTANDARD("DEFAULT")
       )
   noc_clk_a_obuf
     (
      .O                       ( NOC_CLK_AP ),
      .OB                      ( NOC_CLK_AN ),
      .I                       ( noc_clk_a_d )
      );

   data_ds
    #(
        .IN_DATA_WIDTH  (36),
        .OUT_DATA_WIDTH (36)
    )
    U_DATA_DS
    (
        .data_in_p  ( {NOC_TRAIN_sequence_in_p,NOC_SOURCE_credit_return_count_p,NOC_SINK_valid_p,NOC_SINK_beat_p} ),
        .data_in_n  ( {NOC_TRAIN_sequence_in_n,NOC_SOURCE_credit_return_count_n,NOC_SINK_valid_n,NOC_SINK_beat_n} ),
        .data_in_s  ( {noc_train_sequence_in_s,noc_source_credit_return_count_s,noc_sink_valid_s,noc_sink_beat_s} ),
        .data_out_s ( {noc_train_sequence_out_s,noc_sink_credit_count_s,noc_source_valid_s,noc_source_beat_s} ),
        .data_out_p ( {NOC_TRAIN_sequence_out_p,NOC_SINK_credit_count_p,NOC_SOURCE_valid_p,NOC_SOURCE_beat_p} ),
        .data_out_n ( {NOC_TRAIN_sequence_out_n,NOC_SINK_credit_count_n,NOC_SOURCE_valid_n,NOC_SOURCE_beat_n} )
    );

    assign  FPGA_LED = leds_in; 

endmodule // fpga_a
   
   
