module  key_debouncing(
    input    clk,
    input    rst_n,
    input    key_in,
    output   key_out
);

    parameter  [31:0]  DELAY_NUM = 32'd400000;  // 20MHz, 20ms
    // parameter  [31:0]  DELAY_NUM = 32'd50;   // for simulation

    reg          key_in_r1;
    reg          key_in_r2;
    reg          key_in_r3;
    wire         key_in_pos;
    wire         key_in_neg;
    reg          pos_is_detected;
    reg          neg_is_detected;
    reg          key_status;
    reg  [31:0]  delay_cnt;
    wire         delay_done;

    always @(posedge clk or negedge rst_n) begin
        if(!rst_n) begin
	    key_in_r1 <= 'd0;
	    key_in_r2 <= 'd0;
	    key_in_r3 <= 'd0;
	end
	else begin
	    key_in_r1 <= key_in;
	    key_in_r2 <= key_in_r1;
	    key_in_r3 <= key_in_r2;
	end
    end

    assign  key_in_pos = (key_in_r3 == 1'b0) && (key_in_r2 == 1'b1);
    assign  key_in_neg = (key_in_r3 == 1'b1) && (key_in_r2 == 1'b0);

    always @(posedge clk or negedge rst_n) begin
        if(!rst_n)
	    pos_is_detected <= 'd0;
	else if(delay_done == 1'b1)
	    pos_is_detected <= 'd0;
	else if(key_in_pos == 1'b1)
	    pos_is_detected <= 'd1;
	else
	    pos_is_detected <= pos_is_detected;
    end

    always @(posedge clk or negedge rst_n) begin
        if(!rst_n)
	    neg_is_detected <= 'd0;
	else if(delay_done == 1'b1)
	    neg_is_detected <= 'd0;
	else if(key_in_neg == 1'b1)
	    neg_is_detected <= 'd1;
	else
	    neg_is_detected <= neg_is_detected;
    end

    always @(posedge clk or negedge rst_n) begin
        if(!rst_n)
	    delay_cnt <= 'd0;
	else if(delay_done == 1'b1)
	    delay_cnt <= 'd0;
	else if(pos_is_detected || neg_is_detected)
	    delay_cnt <= delay_cnt +'d1;
	else
	    delay_cnt <= delay_cnt;
    end

    assign  delay_done = (delay_cnt == DELAY_NUM) ? 1'b1 : 1'b0;

    always @(posedge clk or negedge rst_n) begin
        if(!rst_n)
	    key_status <= 'd0;
	else if(pos_is_detected && delay_done && (key_in_r3 == 1'b1))
	    key_status <= 'd1;
	else if(neg_is_detected && delay_done && (key_in_r3 == 1'b0))
	    key_status <= 'd0;
	else
	    key_status <= key_status;
    end

    assign  key_out = key_status;
    
    initial begin
        key_status = 'd0;
        delay_cnt = 'd0;
        neg_is_detected = 'd0;
        pos_is_detected = 'd0;
        key_in_r1 = 'd0;
        key_in_r2 = 'd0;
        key_in_r3 = 'd0;
    end

endmodule
